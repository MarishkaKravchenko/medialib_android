package ru.roscha_akademii.medialib.ui.adapters;


import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.model.Book;
import com.squareup.picasso.Picasso;
import ru.roscha_akademii.medialib.databinding.BooksListItemBinding;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ViewHolder> {

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        BooksListItemBinding binding = BooksListItemBinding.inflate(inflater, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Book book = Book.ITEMS[position];
        holder.binding.setBook(book);
        holder.binding.setClick(new BookClickHandler() {
            @Override
            public void onNewClick(View view) {
                Toast.makeText(view.getContext(), "onNewClick", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onWatchedClick(View view) {
                Toast.makeText(view.getContext(), "onWatchedClick", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return Book.ITEMS.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        //генерится по имени xml
        BooksListItemBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }

    @BindingAdapter("bind:imageUrl")
    public static void loadImage(ImageView imageView, String v) {
        Picasso.with(imageView.getContext()).load(v).into(imageView);
    }

    @BindingAdapter("bind:filter")
    public static void applyFilter(ImageView imageView, String v) {
        imageView.setColorFilter(null);
        if("grey".equals(v)){
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);
            ColorMatrixColorFilter cf = new ColorMatrixColorFilter(matrix);
            imageView.setColorFilter(cf);
        }
    }
}

