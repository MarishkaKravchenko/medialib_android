package ru.roscha_akademii.medialib.ui.adapters;


import android.view.View;

public interface BookClickHandler {
    void onNewClick(View view);
    void onWatchedClick(View view);
}
