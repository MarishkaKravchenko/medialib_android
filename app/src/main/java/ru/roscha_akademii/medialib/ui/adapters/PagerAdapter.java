package ru.roscha_akademii.medialib.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ru.roscha_akademii.medialib.ui.fragments.BookListFragment;
import ru.roscha_akademii.medialib.ui.fragments.VideoListFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                BookListFragment tab1 = new BookListFragment();
                return tab1;
            case 1:
                VideoListFragment tab2 = new VideoListFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
