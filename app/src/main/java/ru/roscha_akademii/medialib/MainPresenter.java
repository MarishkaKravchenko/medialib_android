package ru.roscha_akademii.medialib;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;

import ru.roscha_akademii.medialib.ui.MainView;

/**
 * Created by tse on 09/09/16.
 */
public interface MainPresenter extends MvpPresenter<MainView> {
    void helloClicked();
}
